
const { src, dest, parallel, series, watch } = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-image')
const clean = require('gulp-clean');


function cleanDist() {
    return src('dist/*', { read: false })
        .pipe(clean());
}

function browsersync() {
    browserSync.init({
        server: { baseDir: './' },
        notify: false,
        online: true
    })
};

function scripts() {
    return src('src/js/*.js'

    )
        .pipe(concat('src.min.js'))
        .pipe(uglify())
        .pipe(dest('dist/js/'))
        .pipe(browserSync.stream())
};

function styles() {
    return src('./src/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(concat('src.min.css'))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
        .pipe(cleancss({ level: { 1: { specialComments: 0 } } }))
        .pipe(dest('./dist/css/'))
        .pipe(browserSync.stream())
};

function images() {
    return src('src/images/**/*')
        .pipe(imagemin())
        .pipe(dest('dist/images/'))
        .pipe(browserSync.stream())
}

function startwatch() {
    browsersync();
    watch('src/**/*.scss').on('change', series(styles, browserSync.reload));
    watch('src/**/*.js').on('change', series(scripts, browserSync.reload));
    watch('src/images/**/*').on('change', series(images, browserSync.reload));
    watch('./index.html').on('change', browserSync.reload);
};


exports.browsersync = browsersync;
exports.scripts = scripts;
exports.styles = styles;
exports.startwatch = startwatch;
exports.images = images;

exports.dev = series(cleanDist, parallel(styles, scripts, images), startwatch);
exports.build = series(cleanDist, styles, scripts, images);
