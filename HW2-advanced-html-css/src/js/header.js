const open = document.querySelector('.header__nav');
const cross = document.querySelector('.nav__cross');
const burger = document.querySelector('.nav__btnMenu');

burger.addEventListener('click', () => {
    open.classList.toggle('openMenu');
});

cross.addEventListener('click', () => {
    open.classList.toggle('openMenu');
});