
const { src, dest, parallel, series, watch } = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
// const imagemin = require('gulp-imagemin');
const imagemin = require('gulp-image')
const clean = require('gulp-clean');

function cleanDist() {
    return src('dist/*', { read: false })
        .pipe(clean());
}

function browsersync() {
    browserSync.init({
        server: { baseDir: 'dist/' },
        notify: false,
        online: true
    })
};

function scripts() {
    return src('app/js/*.js'

    )
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(dest('dist/js/'))
        .pipe(browserSync.stream())
};

function styles() {
    return src('./app/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(concat('app.min.css'))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
        .pipe(cleancss({ level: { 1: { specialComments: 0 } } }))
        .pipe(dest('./dist/css/'))
        .pipe(browserSync.stream())
};

function images() {
    return src('app/images/**/*')
        .pipe(imagemin())
        .pipe(dest('dist/images/'))

}
function startwatch() {
    browsersync();
    watch('app/**/*.scss').on('change', series(styles, browserSync.reload));
    watch('app/**/*.js').on('change', series(scripts, browserSync.reload));
    watch('*.html').on('change', series(buildHtml, browserSync.reload));
};

function buildHtml() {
    return src('app/index.html')
        .pipe(dest('dist'))
}
exports.buildHtml = buildHtml;
exports.browsersync = browsersync;
exports.scripts = scripts;
exports.styles = styles;
exports.startwatch = startwatch;
exports.images = images;
exports.default = series(cleanDist, parallel(styles, scripts, buildHtml), startwatch);
